package com.example;

public class LuggageException extends Exception{
    public LuggageException(String message) {
        super(message);
    }
}
