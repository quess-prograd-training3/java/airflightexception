package com.example;

import java.util.Scanner;

public class AirFlight {
    private final float maximumWeight=15.0F;
    private final float chargePerKg=500;
    private float personLuggageWeight;

    public void initializeInputs(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter LuggageWeight:- ");
        personLuggageWeight=scanner.nextFloat();
    }
    public void checkLuggageWeight()throws LuggageException{

        if(personLuggageWeight > maximumWeight) {
            if (!((float)(personLuggageWeight - maximumWeight) == 0.10000038f)) {
                throw new LuggageException("Your LuggageWeight Exceed the Limit of 15Kg your extra weight is " + (personLuggageWeight - maximumWeight) + " Kg.");
            }
            else{
               System.out.println("AirBharat wishes a safe and Happy Journey");
            }
        }
        else {
            System.out.println("AirBharat wishes a safe and Happy Journey");
        }
    }

    public void extraChargeOnLuggage(){
        if(personLuggageWeight > maximumWeight){
            System.out.println("ExtraCharge Rupees:- "+(personLuggageWeight-maximumWeight)*chargePerKg);
            System.out.println("AirBharat wishes a safe and Happy Journey");
        }
    }
}
