package com.example;

public class MainAirFlight {
    public static void main(String[] args) {
        AirFlight airFlight=new AirFlight();
        airFlight.initializeInputs();
        try {
            airFlight.checkLuggageWeight();
        }
        catch(LuggageException luggageException){
            System.out.println(luggageException.getMessage());
            airFlight.extraChargeOnLuggage();
        }

    }
}
